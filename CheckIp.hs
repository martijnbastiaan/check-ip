#!/usr/bin/env stack
{- stack
  script
  --resolver nightly-2020-12-14
  --package protolude
  --package http-conduit
  --package co-log,co-log-core
  --package pretty-show
  --package text
  --package ip
  --package process
  --package string-interpolate
  --package telegram-bot-simple
  --package servant-client-core
  --extra-dep bytebuild-0.3.7.0
  --extra-dep byteslice-0.2.4.0
  --extra-dep bytesmith-0.3.7.0
  --extra-dep natural-arithmetic-0.1.2.0
  --extra-dep primitive-offset-0.2.0.0
  --extra-dep primitive-unlifted-0.1.3.0
  --extra-dep run-st-0.1.1.0
  --extra-dep contiguous-0.5.1
  --extra-dep tuples-0.1.0.0
  --ghc-options=-Wall
-}
{-# LANGUAGE ImplicitParams #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE NumericUnderscores #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE QuasiQuotes #-}

-- base
import System.Environment (getEnv)

-- protolude
import Protolude

-- http-conduit
import qualified Network.HTTP.Simple as Http

-- colog
import Colog

-- pretty-show
import Text.Show.Pretty (ppShow)

-- text
import Data.Text as Text

-- ip
import Net.IPv4 (IPv4)
import qualified Net.IPv4 as Ipv4

-- interpolate
import Data.String.Interpolate (i)

-- telegram-bot-simple
import qualified Telegram.Bot.API.Methods as Telegram
import qualified Telegram.Bot.API.MakingRequests as Telegram
import qualified Telegram.Bot.API.Types as Telegram
import Telegram.Bot.API.MakingRequests (Response(..))
import Telegram.Bot.API.Methods (SendMessageRequest(..))

tweakIp :: IPv4
tweakIp = Ipv4.fromOctets 45 135 7 34

ipifyUrl :: Text
ipifyUrl = "https://api.ipify.org"

ipifyReq :: Http.Request
ipifyReq = Http.parseRequest_ (Text.unpack ipifyUrl)

ppShowT :: Show a => a -> Text
ppShowT = Text.pack . ppShow

previousIpFile :: FilePath
previousIpFile = "/dev/shm/previous_ip"

getPreviousIp :: IO (Maybe IPv4)
getPreviousIp = do
  prevIpE <- try @SomeException (readFile previousIpFile)
  case prevIpE of
    Left _ -> pure Nothing
    Right prevIp -> pure (Ipv4.decode prevIp)

setPreviousIp :: IPv4 -> IO ()
setPreviousIp = writeFile previousIpFile . Ipv4.encode

getIpProvider :: IPv4 -> Text
getIpProvider ipv4 =
  if ipv4 == tweakIp then
    "Tweak"
  else
    "Other"

getIp :: (WithLog env Message m, MonadIO m) => m (Maybe IPv4)
getIp = do
  logDebug $ "Querying " <> ipifyUrl <> " for public ip.."
  respE <- liftIO (try @Http.HttpException (Http.httpBS ipifyReq))

  case respE of
    Left err -> do
      logWarning $ "HttpException:\n" <> show err
      pure Nothing
    Right resp -> do
      let
        status = Http.getResponseStatusCode resp
        body = decodeUtf8' (Http.getResponseBody resp)
      case (status, body) of
        (200, Right ip) -> do
          logInfo $ ip
          pure (Ipv4.decode ip)
        (200, Left err) -> do
          logWarning $ "Got non-utf8 response:\n" <> ppShowT err
          logWarning $ "Response was:\n" <> ppShowT resp
          pure Nothing
        _ -> do
          logWarning $ "api.ipify.org down? Got non-200 response: " <> ppShowT resp
          pure Nothing

makeMsg :: (?chatId :: Telegram.SomeChatId) => Text -> Telegram.SendMessageRequest
makeMsg msg = Telegram.SendMessageRequest
  { sendMessageChatId = ?chatId
  , sendMessageText = msg
  , sendMessageParseMode = Nothing
  , sendMessageDisableWebPagePreview = Just True
  , sendMessageDisableNotification = Just False
  , sendMessageReplyToMessageId = Nothing
  , sendMessageReplyMarkup = Nothing }

checkIp ::
  ( WithLog env Message m
  , MonadIO m
  , ?chatId :: Telegram.SomeChatId
  , ?token :: Telegram.Token ) =>
  Maybe IPv4 -> m ()
checkIp Nothing =
  pure ()
checkIp (Just ip) = do
  prevIpM <- liftIO getPreviousIp
  case prevIpM of
    Just prevIp -> do
      let
        ipT = Ipv4.encode ip
        prevIpT = Ipv4.encode prevIp
        ipProvider = getIpProvider ip
        prevIpProvider = getIpProvider prevIp

      when
        (ip /= prevIp)
        (sendMsg 20 [i|
          Changed IP from: #{prevIpT} [#{prevIpProvider}] to #{ipT} [#{ipProvider}]
        |])
    Nothing ->
      pure ()

  liftIO (setPreviousIp ip)

sendMsg ::
  ( WithLog env Message m
  , MonadIO m
  , ?chatId :: Telegram.SomeChatId
  , ?token :: Telegram.Token ) =>
  Int -> Text -> m ()
sendMsg timeout msg = go timeout
 where
  go n = do
    logInfo $ "Sending Telegram message: " <> msg

    let tMsg = Telegram.sendMessage (makeMsg msg)
    reply <- liftIO (Telegram.defaultRunBot ?token tMsg)
    case (n, reply) of
      (0, Left resp) -> do
        logDebug ("Sending message failed:\n" <> ppShowT resp)
        panic "Crashing hard, see you in your INBOX!"
      (0, Right (resp@Response{responseOk=False})) -> do
        logDebug ("Sending message failed:\n" <> ppShowT resp)
        panic "Crashing hard, see you in your INBOX!"
      (_, Right _) ->
        pure ()
      (_, _) -> do
        liftIO (threadDelay 5_000_000) -- 5 seconds
        logInfo $ "Failed to send message! Retrying.. (" <> show n <> " tries left)"
        go (n - 1)

checkTelegramToken ::
  (WithLog env Message m, MonadIO m, ?token :: Telegram.Token) =>
  m ()
checkTelegramToken = do
  logDebug "Checking Telegram token.."
  me <- liftIO (Telegram.defaultRunBot ?token Telegram.getMe)
  case me of
    Left resp -> do
      logDebug ("getMe failed:\n" <> ppShowT resp)
      panic "Fatal error, cannot recover. Is your TELEGRAM_BOT_TOKEN correct?"
    Right _ ->
      logDebug "OK"

main :: IO ()
main = do
  let logAction = cmap fmtMessage logTextStdout
  telegramToken <- Telegram.Token . Text.pack <$> getEnv "TELEGRAM_BOT_TOKEN"
  telegramChatId <-
       Telegram.SomeChatId
    .  Telegram.ChatId
    .  fromMaybe (panic "Could not parse TELEGRAM_CHAT_ID as integer")
    .  readMaybe
   <$> getEnv "TELEGRAM_CHAT_ID"

  let ?token = telegramToken
  let ?chatId = telegramChatId

  () <- usingLoggerT logAction checkTelegramToken

  forever $ do
    () <- usingLoggerT logAction (getIp >>= checkIp)
    threadDelay 5_000_000 -- 5 seconds

